use crate::app::App;
use feedly_api::models::SearchResultItem;
use glib::{
    clone,
    subclass::{self, Signal},
};
use gtk4::{prelude::*, subclass::prelude::*};
use gtk4::{CompositeTemplate, GestureClick, Image, Label};
use news_flash::models::Url;
use once_cell::sync::Lazy;

mod imp {
    use super::*;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(file = "data/resources/ui_templates/discover/search_item.blp")]
    pub struct SearchItemRow {
        #[template_child]
        pub search_item_row_click: TemplateChild<GestureClick>,
        #[template_child]
        pub search_item_title: TemplateChild<Label>,
        #[template_child]
        pub search_item_description: TemplateChild<Label>,
        #[template_child]
        pub search_item_image: TemplateChild<Image>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for SearchItemRow {
        const NAME: &'static str = "SearchItemRow";
        type ParentType = gtk4::ListBoxRow;
        type Type = super::SearchItemRow;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for SearchItemRow {
        fn signals() -> &'static [Signal] {
            static SIGNALS: Lazy<Vec<Signal>> =
                Lazy::new(|| vec![Signal::builder("add-feed").param_types([String::static_type()]).build()]);
            SIGNALS.as_ref()
        }
    }

    impl WidgetImpl for SearchItemRow {}

    impl ListBoxRowImpl for SearchItemRow {}
}

glib::wrapper! {
    pub struct SearchItemRow(ObjectSubclass<imp::SearchItemRow>)
        @extends gtk4::Widget, gtk4::ListBoxRow;
}

impl SearchItemRow {
    pub fn new(item: &SearchResultItem) -> Self {
        let row = glib::Object::new::<Self>();
        let imp = row.imp();

        let search_item_feed_url = Self::feedly_id_to_rss_url(&item.feed_id);
        imp.search_item_row_click.connect_released(clone!(
            #[weak]
            row,
            #[upgrade_or_panic]
            move |_gesture, times, _x, _y| {
                if times != 1 {
                    return;
                }

                row.grab_focus();

                if let Some(search_item_feed_url) = &search_item_feed_url {
                    row.emit_by_name::<()>("add-feed", &[&search_item_feed_url.as_str().to_owned()]);
                }
            }
        ));

        imp.search_item_title.set_label(
            &item
                .title
                .clone()
                .expect("Empty titles should not be created in the first place!"),
        );

        let description = if let Some(description) = &item.description {
            description.replace(['\n', '\r', '_'], " ")
        } else {
            "No description".to_owned()
        };

        imp.search_item_description.set_label(&description);

        let icon_url = if let Some(visual_url) = &item.visual_url {
            Some(visual_url.clone())
        } else if let Some(logo) = &item.logo {
            Some(logo.clone())
        } else {
            item.icon_url.as_ref().cloned()
        };

        if let Some(icon_url) = icon_url {
            let search_item_image = imp.search_item_image.get();

            App::default().execute_with_callback(
                move |_news_flash, client| async move {
                    let bytes = client.get(&icon_url).send().await?.bytes().await?;
                    Ok(bytes)
                },
                move |_app, res: Result<_, reqwest::Error>| {
                    if let Ok(byte_vec) = res {
                        let bytes = glib::Bytes::from_owned(byte_vec);
                        let texture = gdk4::Texture::from_bytes(&bytes);
                        search_item_image.set_paintable(texture.ok().as_ref());
                    }
                },
            );
        }

        row
    }

    fn feedly_id_to_rss_url(feedly_id: &str) -> Option<Url> {
        let url_string: String = feedly_id.chars().skip(5).collect();
        if let Ok(url) = Url::parse(&url_string) {
            Some(url)
        } else {
            None
        }
    }
}
