mod popover_tag_gobject;
mod popover_tag_row;

use crate::app::App;
use crate::undo_delete_action::UndoDelete;
use crate::util::constants;
use gdk4::ModifierType;
use gio::ListStore;
use glib::{clone, subclass, ControlFlow, Propagation};
use gtk4::{
    prelude::*, subclass::prelude::*, CompositeTemplate, ConstantExpression, CustomSorter, EventControllerKey,
    Expression, Label, ListItem, ListView, Popover, PropertyExpression, ScrolledWindow, SearchEntry,
    SignalListItemFactory, SingleSelection, SortListModel, Stack, StringFilter, Widget, INVALID_LIST_POSITION,
};
use news_flash::models::{ArticleID, Tag, TagID};
use popover_tag_gobject::PopoverTagGObject;
use popover_tag_row::PopoverTagRow;
use std::cell::RefCell;
use std::collections::HashSet;

mod imp {
    use super::*;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(file = "data/resources/ui_templates/tagging/popover.blp")]
    pub struct TagPopover {
        #[template_child]
        pub stack: TemplateChild<Stack>,
        #[template_child]
        pub create_label: TemplateChild<Label>,

        #[template_child]
        pub scroll: TemplateChild<ScrolledWindow>,
        #[template_child]
        pub listview: TemplateChild<ListView>,
        #[template_child]
        pub sorter: TemplateChild<CustomSorter>,
        #[template_child]
        pub sort_list: TemplateChild<SortListModel>,
        #[template_child]
        pub filter: TemplateChild<StringFilter>,
        #[template_child]
        pub list_store: TemplateChild<ListStore>,
        #[template_child]
        pub factory: TemplateChild<SignalListItemFactory>,
        #[template_child]
        pub selection: TemplateChild<SingleSelection>,

        #[template_child]
        pub entry: TemplateChild<SearchEntry>,
        #[template_child]
        pub key_event: TemplateChild<EventControllerKey>,

        pub article_id: RefCell<Option<ArticleID>>,
        pub assigned_tags: RefCell<HashSet<TagID>>,
        pub tags: RefCell<Vec<Tag>>,
        pub ctrl_pressed: RefCell<bool>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for TagPopover {
        const NAME: &'static str = "TagPopover";
        type ParentType = Popover;
        type Type = super::TagPopover;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for TagPopover {
        fn constructed(&self) {
            self.obj().init();
        }
    }

    impl WidgetImpl for TagPopover {}

    impl PopoverImpl for TagPopover {}
}

glib::wrapper! {
    pub struct TagPopover(ObjectSubclass<imp::TagPopover>)
        @extends Widget, Popover;
}

impl Default for TagPopover {
    fn default() -> Self {
        Self::new()
    }
}

impl TagPopover {
    pub fn new() -> Self {
        glib::Object::new::<Self>()
    }

    fn init(&self) {
        let imp = self.imp();

        imp.entry.connect_search_changed(clone!(
            #[weak(rename_to = this)]
            self,
            #[upgrade_or_panic]
            move |search_entry| {
                let imp = this.imp();
                let query = search_entry.text();
                let query = query.as_str().trim();
                imp.filter.set_search(Some(query));
                imp.create_label
                    .set_text(&format!("Press Enter to create and assign '{}'", query));

                if imp.sort_list.n_items() == 0 {
                    imp.stack
                        .set_visible_child_name(if query.is_empty() { "empty" } else { "create" });
                } else {
                    imp.stack.set_visible_child_name("list");
                }
            }
        ));

        imp.entry.connect_activate(clone!(
            #[weak(rename_to = this)]
            self,
            #[upgrade_or_panic]
            move |search_entry| {
                let query = search_entry.text();
                let query = query.as_str().trim();

                if query.is_empty() {
                    return;
                }

                let imp = this.imp();
                let article_id = imp.article_id.borrow().clone().take().unwrap();
                let ctrl_pressed = *imp.ctrl_pressed.borrow();
                let no_matches = imp.sort_list.n_items() == 0;
                let exact_match = this.exact_search_match(query);

                if exact_match.is_none() && (ctrl_pressed || no_matches) {
                    // Create new tag
                    App::default().add_tag(constants::TAG_DEFAULT_COLOR.into(), query.into(), Some(article_id));
                    this.update();
                } else if let Some(best_match) = this.best_search_match() {
                    // Assign selected tag
                    let is_assigned = best_match.is_assigned();
                    best_match.set_assigned(!is_assigned);

                    let tag_id = best_match.tag_id();
                    if is_assigned {
                        App::default().untag_article(article_id, tag_id);
                    } else {
                        App::default().tag_article(article_id, tag_id);
                    }
                }
            }
        ));
        imp.key_event.connect_key_pressed(clone!(
            #[weak(rename_to = this)]
            self,
            #[upgrade_or_panic]
            move |_controller, key, _keyval, state| {
                let imp = this.imp();
                let ctrl_pressed = state.contains(ModifierType::CONTROL_MASK);
                imp.ctrl_pressed.replace(ctrl_pressed);

                if ctrl_pressed && key == gdk4::Key::Return {
                    imp.entry.emit_activate();
                } else if key == gdk4::Key::Escape {
                    this.popdown();
                }
                Propagation::Proceed
            }
        ));

        imp.factory.connect_setup(move |_factory, list_item| {
            let row = PopoverTagRow::new();

            let list_item = list_item.downcast_ref::<ListItem>().unwrap();
            list_item.set_child(Some(&row));

            // Create expression describing `list_item->item`
            let list_item_expression = ConstantExpression::new(list_item);
            let article_gobject_expression =
                PropertyExpression::new(ListItem::static_type(), Some(&list_item_expression), "item");

            // Update title
            let title_expression = PropertyExpression::new(
                PopoverTagGObject::static_type(),
                Some(&article_gobject_expression),
                "title",
            );
            title_expression.bind(&row, "title", Some(&row));

            // Update color
            let color_expression = PropertyExpression::new(
                PopoverTagGObject::static_type(),
                Some(&article_gobject_expression),
                "color",
            );
            color_expression.bind(&row, "color", Some(&row));

            // Update assigned
            let assigned_expression = PropertyExpression::new(
                PopoverTagGObject::static_type(),
                Some(&article_gobject_expression),
                "assigned",
            );
            assigned_expression.bind(&row, "assigned", Some(&row));
        });
        imp.factory.connect_bind(|_factory, list_item| {
            let list_item = list_item.downcast_ref::<ListItem>().unwrap();
            let tag = list_item.item().and_downcast::<PopoverTagGObject>().unwrap();
            let child = list_item.child().and_downcast::<PopoverTagRow>().unwrap();
            child.bind_model(&tag);
        });
        imp.sorter.set_sort_func(move |obj1, obj2| {
            let tag_1: &PopoverTagGObject = obj1.downcast_ref::<PopoverTagGObject>().unwrap();
            let tag_2: &PopoverTagGObject = obj2.downcast_ref::<PopoverTagGObject>().unwrap();

            let assigned_1 = tag_1.is_assigned();
            let assigned_2 = tag_2.is_assigned();

            let title_1 = tag_1.title();
            let title_2 = tag_2.title();

            if assigned_1 == assigned_2 {
                title_1.cmp(&title_2).into()
            } else {
                assigned_1.cmp(&assigned_2).reverse().into()
            }
        });

        let expr: Option<&Expression> = None;
        let title_expression = PropertyExpression::new(PopoverTagGObject::static_type(), expr, "title");
        imp.filter.set_expression(Some(&title_expression));

        imp.listview.connect_activate(clone!(
            #[weak(rename_to = this)]
            self,
            #[upgrade_or_panic]
            move |list_view, position| {
                let imp = this.imp();
                let model = list_view.model().expect("The model has to exist.");
                let tag_gobject = model
                    .item(position)
                    .expect("The item has to exist.")
                    .downcast::<PopoverTagGObject>()
                    .expect("The item has to be an `PopoverTagGObject`.");

                let is_assigned = tag_gobject.is_assigned();
                tag_gobject.set_assigned(!is_assigned);
                if let Some(article_id) = imp.article_id.borrow().as_ref() {
                    let tag_id = tag_gobject.tag_id();
                    if is_assigned {
                        App::default().untag_article(article_id.clone(), tag_id);
                    } else {
                        App::default().tag_article(article_id.clone(), tag_id);
                    }
                };
            }
        ));

        self.connect_show(|this| {
            this.update();
        });
    }

    fn reset_view(&self) {
        let imp = self.imp();
        imp.entry.set_text("");
        if self.is_visible() {
            let _ = imp.entry.grab_focus();
        }

        if imp.list_store.n_items() == 0 {
            imp.stack.set_visible_child_name("empty");
        } else {
            imp.stack.set_visible_child_name("list");
        }

        let scroll = imp.scroll.clone();
        glib::timeout_add_local(std::time::Duration::from_millis(20), move || {
            scroll.vadjustment().set_value(0.0);
            ControlFlow::Break
        });
    }

    pub fn set_article_id(&self, article_id: &ArticleID) {
        let imp = self.imp();
        imp.article_id.replace(Some(article_id.clone()));
        self.update();
    }

    fn update(&self) {
        let imp = self.imp();
        imp.list_store.remove_all();
        imp.assigned_tags.borrow_mut().clear();

        let processing_undo_delete_actions = App::default().processing_undo_delete_actions();
        let mut blacklisted_tags: HashSet<TagID> = processing_undo_delete_actions
            .borrow()
            .iter()
            .filter_map(|action| {
                if let UndoDelete::Tag(tag_id, _) = action {
                    Some(tag_id.clone())
                } else {
                    None
                }
            })
            .collect();
        if let Some(UndoDelete::Tag(tag_id, _)) = App::default().current_undo_delete_action() {
            blacklisted_tags.insert(tag_id);
        }

        let article_id = imp.article_id.borrow().clone();
        App::default().execute_with_callback(
            |news_flash, _client| async move {
                if let Some(news_flash) = news_flash.read().await.as_ref() {
                    let article_tags = article_id.and_then(|id| news_flash.get_tags_of_article(&id).ok());
                    let all_tags = news_flash.get_tags().ok().map(|(tags, _taggings)| tags);

                    (article_tags, all_tags)
                } else {
                    (None, None)
                }
            },
            clone!(
                #[weak(rename_to = this)]
                self,
                #[upgrade_or_panic]
                move |_app, (article_tags, all_tags): (Option<Vec<Tag>>, Option<Vec<Tag>>)| {
                    let imp = this.imp();

                    if let Some(article_tags) = article_tags {
                        for tag in &article_tags {
                            if blacklisted_tags.contains(&tag.tag_id) {
                                continue;
                            }
                            imp.assigned_tags.borrow_mut().insert(tag.tag_id.clone());
                        }
                    }

                    if let Some(tags) = all_tags {
                        for tag in tags {
                            imp.list_store.append(&PopoverTagGObject::from_model(
                                &tag,
                                imp.assigned_tags.borrow().contains(&tag.tag_id),
                            ));
                            imp.tags.borrow_mut().push(tag);
                        }
                    }

                    this.reset_view();
                }
            ),
        );
    }

    fn exact_search_match(&self, search_query: &str) -> Option<PopoverTagGObject> {
        let imp = self.imp();
        let n_items = imp.sort_list.n_items();

        for i in 0..n_items {
            if let Some(tag_gobject) = imp
                .sort_list
                .item(i)
                .as_ref()
                .map(move |obj| obj.downcast_ref::<PopoverTagGObject>().unwrap())
            {
                if tag_gobject.title() == search_query {
                    return Some(tag_gobject.clone());
                }
            }
        }
        None
    }

    fn best_search_match(&self) -> Option<PopoverTagGObject> {
        let imp = self.imp();
        let selected = imp.selection.selected();
        let selected = if selected == INVALID_LIST_POSITION { 0 } else { selected };

        imp.sort_list
            .item(selected)
            .map(move |obj| obj.downcast_ref::<PopoverTagGObject>().unwrap().clone())
    }
}
