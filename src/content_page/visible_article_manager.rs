use crate::article_list::ReadUpdate;
use crate::{app::App, main_window_actions::MainWindowActions, util::constants};
use news_flash::models::{ArticleID, Enclosure, FatArticle, Marked, Read};
use std::cell::RefCell;
use std::rc::Rc;

#[derive(Debug, Default)]
pub struct VisibleArticleManager {
    article: Rc<RefCell<Option<FatArticle>>>,
    enclosures: Rc<RefCell<Option<Vec<Enclosure>>>>,
}

impl VisibleArticleManager {
    pub fn new() -> Self {
        Self {
            article: Rc::new(RefCell::new(None)),
            enclosures: Rc::new(RefCell::new(None)),
        }
    }

    pub fn load_article_from_db(&self, article_id: ArticleID, set_read: bool) {
        App::default()
            .content_page_state()
            .borrow_mut()
            .set_prefer_scraped_content(true);

        let visible_article = self.article.clone();
        let visible_enclosures = self.enclosures.clone();

        App::default().execute_with_callback(
            move |news_flash, _client| async move {
                if let Some(news_flash) = news_flash.read().await.as_ref() {
                    if let Ok(fat_article) = news_flash.get_fat_article(&article_id) {
                        let enclosures = news_flash.get_enclosures(&article_id).ok().and_then(|enclosures| {
                            if enclosures.is_empty() {
                                None
                            } else {
                                Some(enclosures)
                            }
                        });
                        let feed_title = news_flash
                            .get_feeds()
                            .ok()
                            .and_then(|(feeds, _)| feeds.into_iter().find(|f| f.feed_id == fat_article.feed_id))
                            .map(|f| f.label)
                            .unwrap_or(constants::UNKNOWN_FEED.into());
                        Some((fat_article, feed_title, enclosures))
                    } else {
                        None
                    }
                } else {
                    None
                }
            },
            move |app, res| {
                if let Some((fat_article, feed_title, enclosures)) = res {
                    let main_window = app.main_window();
                    let content_page = main_window.content_page();
                    let article_view_column = content_page.articleview_column();
                    let article_id = fat_article.article_id.clone();
                    let unread = fat_article.unread;

                    article_view_column.show_article(
                        &article_id,
                        if set_read { Read::Read } else { unread },
                        fat_article.marked,
                        fat_article.scraped_content.is_some(),
                        enclosures.as_ref(),
                    );
                    article_view_column
                        .article_view()
                        .show_article(fat_article.clone(), feed_title);

                    content_page
                        .state()
                        .borrow_mut()
                        .set_visible_article_id(Some(&article_id));

                    visible_article.borrow_mut().replace(fat_article);
                    visible_enclosures.replace(enclosures);

                    if set_read && unread == Read::Unread {
                        App::default().mark_article_read(ReadUpdate {
                            article_id,
                            read: Read::Read,
                        });
                    }

                    let show_article_view = content_page
                        .state()
                        .borrow_mut()
                        .get_article_view_visible()
                        .unwrap_or(true);
                    content_page.responsive_layout().show_article_view(show_article_view);
                    MainWindowActions::update_state();
                }
            },
        );
    }

    pub fn refresh_article_metadata_from_db(&self) {
        let visible_article_id = if let Some(visible_article) = self.article.borrow().as_ref() {
            visible_article.article_id.clone()
        } else {
            return;
        };

        let enclosures = self.enclosures.clone();
        let visible_article = self.article.clone();

        App::default().execute_with_callback(
            |news_flash, _client| async move {
                if let Some(news_flash) = news_flash.read().await.as_ref() {
                    news_flash.get_fat_article(&visible_article_id).ok()
                } else {
                    None
                }
            },
            move |app, updated_visible_article| {
                if let Some(updated_visible_article) = updated_visible_article {
                    app.main_window().content_page().articleview_column().show_article(
                        &updated_visible_article.article_id,
                        updated_visible_article.unread,
                        updated_visible_article.marked,
                        updated_visible_article.scraped_content.is_some(),
                        enclosures.borrow().as_ref(),
                    );

                    if let Some(article) = visible_article.borrow_mut().as_mut() {
                        article.unread = updated_visible_article.unread;
                        article.marked = updated_visible_article.marked;
                    }
                }
            },
        );
    }

    pub fn get_article(&self) -> Option<FatArticle> {
        self.article.borrow().as_ref().cloned()
    }

    pub fn get_enclosures(&self) -> Option<Vec<Enclosure>> {
        self.enclosures.borrow().clone()
    }

    pub fn update_enclosure_position(&self, updated_enclosure: &Enclosure) {
        if let Some(enclosures) = self.enclosures.borrow_mut().as_mut() {
            for enclosure in enclosures.iter_mut() {
                if enclosure.url == updated_enclosure.url {
                    enclosure.position = updated_enclosure.position;
                    break;
                }
            }

            App::default()
                .main_window()
                .content_page()
                .articleview_column()
                .update_enclosures(enclosures);
        }
    }

    pub fn set_read(&self, read: Read) {
        let (article_id, unread, marked, have_scraped_content) =
            if let Some(article) = self.article.borrow_mut().as_mut() {
                article.unread = read;

                (
                    article.article_id.clone(),
                    article.unread,
                    article.marked,
                    article.scraped_content.is_some(),
                )
            } else {
                return;
            };

        App::default()
            .main_window()
            .content_page()
            .articleview_column()
            .show_article(
                &article_id,
                unread,
                marked,
                have_scraped_content,
                self.enclosures.borrow().as_ref(),
            );
    }

    pub fn set_marked(&self, marked: Marked) {
        let (article_id, unread, marked, have_scraped_content) =
            if let Some(article) = self.article.borrow_mut().as_mut() {
                article.marked = marked;

                (
                    article.article_id.clone(),
                    article.unread,
                    article.marked,
                    article.scraped_content.is_some(),
                )
            } else {
                return;
            };

        App::default()
            .main_window()
            .content_page()
            .articleview_column()
            .show_article(
                &article_id,
                unread,
                marked,
                have_scraped_content,
                self.enclosures.borrow().as_ref(),
            );
    }

    pub fn clear(&self) {
        self.article.take();
        self.enclosures.take();
        App::default()
            .content_page_state()
            .borrow_mut()
            .set_prefer_scraped_content(false);
        App::default()
            .main_window()
            .content_page()
            .articleview_column()
            .clear_article();
    }
}
