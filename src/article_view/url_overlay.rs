use gtk4::{prelude::*, subclass::prelude::*, CompositeTemplate};
use gtk4::{Box, Label, Widget};

mod imp {
    use super::*;
    use glib::subclass;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(file = "data/resources/ui_templates/article_view/url.blp")]
    pub struct UrlOverlay {
        #[template_child]
        pub label: TemplateChild<Label>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for UrlOverlay {
        const NAME: &'static str = "UrlOverlay";
        type ParentType = Box;
        type Type = super::UrlOverlay;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for UrlOverlay {}

    impl WidgetImpl for UrlOverlay {}

    impl BoxImpl for UrlOverlay {}
}

glib::wrapper! {
    pub struct UrlOverlay(ObjectSubclass<imp::UrlOverlay>)
        @extends Widget, Box;
}

impl Default for UrlOverlay {
    fn default() -> Self {
        glib::Object::new::<Self>()
    }
}

impl UrlOverlay {
    pub fn new() -> Self {
        Self::default()
    }

    pub fn set_url(&self, uri: Option<glib::GString>) {
        let imp = self.imp();

        if let Some(uri) = uri.as_ref() {
            imp.label.set_label(uri.as_str());
        }

        self.set_visible(uri.is_some());
    }
}
