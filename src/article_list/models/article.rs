use crate::util::constants;
use chrono::{DateTime, TimeDelta, Utc};
use diffus::{Diffable, Same};
use news_flash::models::{Article, ArticleID, Feed, FeedID, Marked, Read, Tag};
use std::sync::Arc;

#[derive(Debug, Clone)]
pub struct ArticleListArticleModel {
    // immutable data
    pub id: ArticleID,
    pub title: Arc<String>,
    pub feed_id: FeedID,

    pub summary: Arc<String>,

    // mutable data
    pub date: DateTime<Utc>,
    pub read: Read,
    pub marked: Marked,
    pub tags: Vec<Tag>,
    pub thumbnail_url: Option<String>,
    pub feed_title: String,
}

impl ArticleListArticleModel {
    pub fn new(article: Article, feed: Option<&Feed>, tags: Vec<&Tag>) -> Self {
        let Article {
            article_id,
            title,
            author: _,
            feed_id,
            date,
            summary,
            direction: _,
            marked,
            thumbnail_url,
            url: _,
            synced: _,
            unread,
        } = article;

        ArticleListArticleModel {
            id: article_id,
            title: Arc::new(match title {
                Some(title) => title,
                None => constants::NO_TITLE.to_owned(),
            }),
            feed_id,
            feed_title: feed.map(|f| f.label.clone()).unwrap_or(constants::UNKNOWN_FEED.into()),
            summary: Arc::new(match summary {
                Some(summary) => summary,
                None => "No Summary".to_owned(),
            }),
            date,
            read: unread,
            marked,
            tags: tags.into_iter().cloned().collect(),
            thumbnail_url,
        }
    }

    fn tags_eq(&self, other_tags: &[Tag]) -> bool {
        self.tags
            .iter()
            .enumerate()
            .all(|(i, tag)| Self::tag_eq(tag, other_tags.get(i)))
    }

    fn tag_eq(tag: &Tag, other: Option<&Tag>) -> bool {
        if let Some(other) = other {
            tag.tag_id == other.tag_id && tag.color == other.color && tag.label == other.label
        } else {
            false
        }
    }
}

impl PartialEq for ArticleListArticleModel {
    fn eq(&self, other: &ArticleListArticleModel) -> bool {
        self.id == other.id
    }
}

impl Same for ArticleListArticleModel {
    fn same(&self, other: &Self) -> bool {
        self.id == other.id
    }
}

impl<'a> Diffable<'a> for ArticleListArticleModel {
    type Diff = ArticleDiff;

    fn diff(&'a self, other: &'a Self) -> diffus::edit::Edit<Self> {
        let date = if self.date == other.date {
            let diff_days = Utc::now().date_naive() - self.date.date_naive();
            if diff_days > TimeDelta::try_days(0).unwrap() && diff_days <= TimeDelta::try_days(1).unwrap() {
                Some(other.date)
            } else {
                None
            }
        } else {
            Some(other.date)
        };

        let read = if self.read == other.read {
            None
        } else {
            Some(other.read)
        };

        let marked = if self.marked == other.marked {
            None
        } else {
            Some(other.marked)
        };

        let tags = if self.tags_eq(&other.tags) {
            None
        } else {
            Some(other.tags.clone())
        };

        let thumbnail = if self.thumbnail_url == other.thumbnail_url {
            None
        } else {
            Some(other.thumbnail_url.clone())
        };

        let feed_title = if self.feed_title == other.feed_title {
            None
        } else {
            Some(other.feed_title.clone())
        };

        if self == other
            && date.is_none()
            && read.is_none()
            && marked.is_none()
            && tags.is_none()
            && thumbnail.is_none()
            && feed_title.is_none()
        {
            diffus::edit::Edit::Copy(self)
        } else {
            diffus::edit::Edit::Change(ArticleDiff {
                id: self.id.clone(),
                read,
                marked,
                date,
                tags,
                thumbnail,
                feed_title,
            })
        }
    }
}

#[derive(Debug, Clone)]
pub struct ArticleDiff {
    pub id: ArticleID,
    pub read: Option<Read>,
    pub marked: Option<Marked>,
    pub date: Option<DateTime<Utc>>,
    pub tags: Option<Vec<Tag>>,
    pub thumbnail: Option<Option<String>>,
    pub feed_title: Option<String>,
}
