use crate::{app::App, util::GtkUtil};
use glib::{clone, subclass, SignalHandlerId};
use gtk4::{prelude::*, subclass::prelude::*, Button, CompositeTemplate, Label};
use libadwaita::EntryRow;
use news_flash::{
    models::{ApiSecret, LoginGUI, PluginID, PluginInfo},
    NewsFlash,
};
use std::cell::RefCell;

mod imp {
    use super::*;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(file = "data/resources/ui_templates/login/custom_api_secret.blp")]
    pub struct CustomApiSecret {
        #[template_child]
        pub info_text: TemplateChild<Label>,
        #[template_child]
        pub client_id_entry: TemplateChild<EntryRow>,
        #[template_child]
        pub client_secret_entry: TemplateChild<EntryRow>,
        #[template_child]
        pub submit_button: TemplateChild<Button>,
        #[template_child]
        pub ignore_button: TemplateChild<Button>,

        pub client_id_entry_signal: RefCell<Option<SignalHandlerId>>,
        pub client_secret_entry_signal: RefCell<Option<SignalHandlerId>>,
        pub plugin_id: RefCell<Option<PluginID>>,
        pub api_secret: RefCell<Option<ApiSecret>>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for CustomApiSecret {
        const NAME: &'static str = "CustomApiSecret";
        type ParentType = gtk4::Box;
        type Type = super::CustomApiSecret;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for CustomApiSecret {
        fn constructed(&self) {
            self.obj().init();
        }
    }

    impl WidgetImpl for CustomApiSecret {}

    impl BoxImpl for CustomApiSecret {}
}

glib::wrapper! {
    pub struct CustomApiSecret(ObjectSubclass<imp::CustomApiSecret>)
        @extends gtk4::Widget, gtk4::Box;
}

impl Default for CustomApiSecret {
    fn default() -> Self {
        Self::new()
    }
}

impl CustomApiSecret {
    pub fn new() -> Self {
        glib::Object::new::<Self>()
    }

    fn init(&self) {
        self.imp()
            .client_id_entry_signal
            .replace(Some(self.setup_entry(&self.imp().client_id_entry)));
        self.imp()
            .client_secret_entry_signal
            .replace(Some(self.setup_entry(&self.imp().client_secret_entry)));

        self.imp().submit_button.connect_clicked(clone!(
            #[weak(rename_to = this)]
            self,
            #[upgrade_or_panic]
            move |_button| {
                if let Some(plugin_id) = this.imp().plugin_id.borrow().as_ref() {
                    if let Some(plugin_info) = NewsFlash::list_backends().get(plugin_id) {
                        let secret = this.imp().api_secret.borrow().clone();
                        App::default()
                            .main_window()
                            .show_web_login_page(plugin_info, secret.as_ref());
                    }
                };
            }
        ));
        self.imp().ignore_button.connect_clicked(clone!(
            #[weak(rename_to = this)]
            self,
            #[upgrade_or_panic]
            move |_button| {
                if let Some(plugin_id) = this.imp().plugin_id.borrow().as_ref() {
                    if let Some(plugin_info) = NewsFlash::list_backends().get(plugin_id) {
                        App::default().main_window().show_web_login_page(plugin_info, None);
                    }
                };
            }
        ));
    }

    pub fn fill(&self, secret: &ApiSecret) {
        self.imp().client_id_entry.set_text(&secret.client_id);
        self.imp().client_secret_entry.set_text(&secret.client_secret);
    }

    pub fn set_service(&self, info: &PluginInfo) {
        self.imp().plugin_id.replace(Some(info.id.clone()));

        if let LoginGUI::OAuth(oauth_info) = &info.login_gui {
            if let Some(create_secret_url) = &oauth_info.create_secret_url {
                self.imp()
                    .info_text
                    .set_markup(
                        &format!(
                            r#"This service uses API secrets to limit the amount of actions per day for every user of the app. You can generate your own secrets <a href="{}">here</a>. This way you don't have to share the limited amount of actions with other users of NewsFlash."#,
                            create_secret_url.as_str()
                        )
                    );
            }
        }
    }

    pub fn reset(&self) {
        self.imp().client_id_entry.set_text("");
        self.imp().client_secret_entry.set_text("");
    }

    fn setup_entry(&self, entry: &EntryRow) -> SignalHandlerId {
        entry.connect_text_notify(clone!(
            #[weak(rename_to = page)]
            self,
            #[upgrade_or_panic]
            move |_entry| {
                let complete = !GtkUtil::is_entry_row_emty(&page.imp().client_id_entry)
                    && !GtkUtil::is_entry_row_emty(&page.imp().client_secret_entry);
                page.imp().submit_button.set_sensitive(complete);

                if complete {
                    page.imp().api_secret.replace(Some(ApiSecret {
                        client_id: page.imp().client_id_entry.text().as_str().into(),
                        client_secret: page.imp().client_secret_entry.text().as_str().into(),
                    }));
                }
            }
        ))
    }
}
