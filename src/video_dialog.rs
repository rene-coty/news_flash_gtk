use crate::{
    app::App,
    util::{constants, GtkUtil},
};
use clapper::{MediaItem, Mpris, PlayerState};
use clapper_gtk::{Billboard, SimpleControls, Video};
use glib::{clone, subclass, ControlFlow, Propagation, SourceId};
use gtk4::{prelude::*, subclass::prelude::*, Box, Button, CallbackAction, CompositeTemplate, Shortcut, Widget};
use libadwaita::{subclass::prelude::*, Bin};
use news_flash::models::Enclosure;
use std::rc::Rc;
use std::{
    cell::{Cell, RefCell},
    time::Duration,
};

mod imp {
    use super::*;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(file = "data/resources/ui_templates/media/video_dialog.blp")]
    pub struct VideoDialog {
        #[template_child]
        pub video: TemplateChild<Video>,
        #[template_child]
        pub controls: TemplateChild<SimpleControls>,
        #[template_child]
        pub back: TemplateChild<Button>,
        #[template_child]
        pub unfullscreen_shortcut: TemplateChild<Shortcut>,
        #[template_child]
        pub skip_shortcut: TemplateChild<Shortcut>,
        #[template_child]
        pub rewind_shortcut: TemplateChild<Shortcut>,
        #[template_child]
        pub top_bar: TemplateChild<Box>,
        #[template_child]
        pub billboard: TemplateChild<Billboard>,

        pub can_seek: Rc<Cell<bool>>,

        pub skip_cooldown_source: Rc<RefCell<Option<SourceId>>>,
        pub skip_seconds: Rc<Cell<f64>>,

        pub rewind_cooldown_source: Rc<RefCell<Option<SourceId>>>,
        pub rewind_seconds: Rc<Cell<f64>>,

        pub enclosure: Rc<RefCell<Option<Enclosure>>>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for VideoDialog {
        const NAME: &'static str = "VideoDialog";
        type ParentType = Bin;
        type Type = super::VideoDialog;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for VideoDialog {
        fn constructed(&self) {
            self.obj().init();
        }
    }

    impl WidgetImpl for VideoDialog {}

    impl BinImpl for VideoDialog {}
}

glib::wrapper! {
    pub struct VideoDialog(ObjectSubclass<imp::VideoDialog>)
        @extends Widget, Bin;
}

impl Default for VideoDialog {
    fn default() -> Self {
        glib::Object::new::<Self>()
    }
}

impl VideoDialog {
    pub fn new() -> Self {
        Self::default()
    }

    fn init(&self) {
        let imp = self.imp();

        imp.back.connect_clicked(|_| {
            App::default().main_window().pop_video();
        });

        imp.skip_shortcut.set_action(Some(CallbackAction::new(clone!(
            #[weak(rename_to = this)]
            self,
            #[upgrade_or_panic]
            move |_widget, _| {
                if this.can_seek() {
                    this.start_skip_cooldown();
                }
                Propagation::Stop
            }
        ))));

        imp.rewind_shortcut.set_action(Some(CallbackAction::new(clone!(
            #[weak(rename_to = this)]
            self,
            #[upgrade_or_panic]
            move |_widget, _| {
                if this.can_seek() {
                    this.start_rewind_cooldown();
                }
                Propagation::Stop
            }
        ))));

        imp.unfullscreen_shortcut
            .set_action(Some(CallbackAction::new(|_widget, _| {
                let window = App::default().main_window();

                if window.is_fullscreen() {
                    window.set_fullscreened(false);
                    Propagation::Stop
                } else {
                    Propagation::Proceed
                }
            })));

        if let Some(player) = imp.video.player() {
            let billboard = imp.billboard.clone();

            player.connect_state_notify(move |player| {
                if player.state() == PlayerState::Paused {
                    billboard.pin_message("media-playback-pause-symbolic", "Paused");
                } else {
                    billboard.unpin_pinned_message();
                }
            });

            let can_seek = imp.can_seek.clone();
            player.connect_seek_done(move |_player| {
                can_seek.set(true);
            });

            let mpris = Mpris::new(
                "org.mpris.MediaPlayer2.Newsflash",
                "Newsflash",
                Some("io.gitlab.news_flash.NewsFlash"),
            );

            player.add_feature(&mpris);
        }

        imp.video.connect_toggle_fullscreen(move |_video| {
            let window = App::default().main_window();
            window.set_fullscreened(!window.is_fullscreen());
        });
    }

    pub fn play(&self, enclosure: &Enclosure) {
        let imp = self.imp();

        imp.enclosure.replace(Some(enclosure.clone()));
        let item = MediaItem::new(enclosure.url.as_str());

        if let Some(player) = imp.video.player() {
            if let Some(queue) = player.queue() {
                queue.add_item(&item);
                queue.select_item(Some(&item));
            }

            player.play();

            if let Some(position) = enclosure.position {
                player.seek(position);
                imp.can_seek.set(false);
            }
        }
    }

    pub fn stop(&self) {
        let imp = self.imp();
        imp.can_seek.set(true);

        if let Some(player) = imp.video.player() {
            let position = player.position();

            let is_finished = player
                .queue()
                .and_then(|q| q.current_item())
                .map(|item| item.duration())
                .map(|duration| position / duration >= constants::VIDEO_END_THRESHOLD_PERCENT)
                .unwrap_or(false);

            let position = if is_finished { None } else { Some(position) };

            self.set_enclosure_position(position);
            player.stop();
        }
    }

    fn set_enclosure_position(&self, position: Option<f64>) {
        if let Some(mut enclosure) = self.imp().enclosure.take() {
            enclosure.position = position;

            App::default()
                .main_window()
                .content_page()
                .visible_article_manager()
                .update_enclosure_position(&enclosure);

            App::default().execute(|news_flash, _clinet| async move {
                if let Some(news_flash) = news_flash.read().await.as_ref() {
                    _ = news_flash.update_enclosure(&enclosure);
                }
            });
        }
    }

    pub fn show_top_bar(&self, show: bool) {
        self.imp().top_bar.set_visible(show);
    }

    fn start_skip_cooldown(&self) {
        let imp = self.imp();

        GtkUtil::remove_source(imp.skip_cooldown_source.take());
        imp.skip_seconds
            .set(imp.skip_seconds.get() + constants::VIDEO_SEEK_SECONDS);
        self.show_skip_overlay();

        imp.skip_cooldown_source.borrow_mut().replace(glib::timeout_add_local(
            Duration::from_millis(constants::VIDEO_SEEK_COOLDOWN_MS),
            clone!(
                #[weak(rename_to = this)]
                self,
                #[upgrade_or_panic]
                move || {
                    this.imp().skip_cooldown_source.take();
                    this.skip();
                    ControlFlow::Break
                }
            ),
        ));
    }

    fn skip(&self) {
        let imp = self.imp();

        if let Some(player) = imp.video.player() {
            let position = player.position();
            player.seek(position + imp.skip_seconds.get());

            imp.can_seek.set(false);
            imp.skip_seconds.set(0.0);
        }
    }

    fn show_skip_overlay(&self) {
        let imp = self.imp();

        let message = format!("+{}s", imp.skip_seconds.get());
        imp.billboard.post_message("media-seek-forward-symbolic", &message);
    }

    fn start_rewind_cooldown(&self) {
        let imp = self.imp();

        GtkUtil::remove_source(imp.rewind_cooldown_source.take());
        imp.rewind_seconds
            .set(imp.rewind_seconds.get() + constants::VIDEO_SEEK_SECONDS);
        self.show_rewind_overlay();

        imp.rewind_cooldown_source.borrow_mut().replace(glib::timeout_add_local(
            Duration::from_millis(constants::VIDEO_SEEK_COOLDOWN_MS),
            clone!(
                #[weak(rename_to = this)]
                self,
                #[upgrade_or_panic]
                move || {
                    this.imp().rewind_cooldown_source.take();
                    this.rewind();
                    ControlFlow::Break
                }
            ),
        ));
    }

    fn rewind(&self) {
        let imp = self.imp();

        if let Some(player) = imp.video.player() {
            let position = player.position();
            player.seek(position - imp.rewind_seconds.get());

            imp.can_seek.set(false);
            imp.rewind_seconds.set(0.0);
        }
    }

    fn show_rewind_overlay(&self) {
        let imp = self.imp();

        let message = format!("-{}s", imp.rewind_seconds.get());
        imp.billboard.post_message("media-seek-backward-symbolic", &message);
    }

    fn can_seek(&self) -> bool {
        self.imp().can_seek.get()
    }
}
