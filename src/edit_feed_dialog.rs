use crate::i18n::i18n;
use gio::ListStore;
use glib::{clone, subclass, Object, ParamSpec, Properties, Value};
use glib::{prelude::*, Propagation};
use gtk4::PropertyExpression;
use gtk4::{subclass::prelude::*, CompositeTemplate, Switch, Widget};
use libadwaita::{prelude::*, subclass::prelude::*, ComboRow, Dialog, EntryRow};
use news_flash::models::{Category, CategoryID, Feed, FeedMapping, PluginCapabilities, NEWSFLASH_TOPLEVEL};
use std::cell::{Ref, RefCell};

use crate::app::App;

mod imp {
    use super::*;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(file = "data/resources/ui_templates/edit_dialogs/feed.blp")]
    pub struct EditFeedDialog {
        #[template_child]
        pub feed_name: TemplateChild<EntryRow>,
        #[template_child]
        pub feed_url: TemplateChild<EntryRow>,
        #[template_child]
        pub category: TemplateChild<ComboRow>,
        #[template_child]
        pub inline_delimiter: TemplateChild<EntryRow>,
        #[template_child]
        pub scrap_content_switch: TemplateChild<Switch>,

        pub current_mapping: RefCell<Option<FeedMapping>>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for EditFeedDialog {
        const NAME: &'static str = "EditFeedDialog";
        type Type = super::EditFeedDialog;
        type ParentType = Dialog;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for EditFeedDialog {}

    impl WidgetImpl for EditFeedDialog {}

    impl AdwDialogImpl for EditFeedDialog {}
}

glib::wrapper! {
    pub struct EditFeedDialog(ObjectSubclass<imp::EditFeedDialog>)
        @extends Widget, Dialog;
}

impl EditFeedDialog {
    pub fn new(feed: Feed, mapping: FeedMapping, categories: Vec<Category>) -> Self {
        let current_category_id = mapping.category_id.clone();
        let dialog: EditFeedDialog = glib::Object::new();
        let imp = dialog.imp();
        imp.current_mapping.replace(Some(mapping));
        imp.feed_name.set_text(&feed.label);

        imp.feed_url
            .set_sensitive(App::default().features().contains(PluginCapabilities::EDIT_FEED_URLS));
        imp.feed_url
            .set_text(&feed.feed_url.as_ref().map(ToString::to_string).unwrap_or_default());
        imp.feed_url.connect_apply(clone!(
            #[strong]
            feed,
            move |entry| {
                App::default().edit_feed_url(&feed, entry.text().as_str());
            }
        ));

        imp.feed_name.connect_apply(clone!(
            #[strong]
            feed,
            move |entry| {
                App::default().rename_feed(&feed, entry.text().as_str());
            }
        ));
        dialog.setup_category_selector(current_category_id, categories);

        let support_mutation = App::default().features().support_mutation();
        imp.feed_name.set_sensitive(support_mutation);
        imp.category.set_sensitive(support_mutation);

        dialog.setup_mathjax_widgets(&feed);
        dialog.setup_scrap_content_widgets(&feed);
        dialog
    }

    fn setup_category_selector(&self, current_category_id: CategoryID, categories: Vec<Category>) {
        let imp = self.imp();
        let list_store = ListStore::new::<CategorySelectGObject>();
        list_store.append(&CategorySelectGObject::new(&NEWSFLASH_TOPLEVEL, &i18n("None")));
        for category in &categories {
            list_store.append(&CategorySelectGObject::from_category(category));
        }

        let expression = PropertyExpression::new(
            CategorySelectGObject::static_type(),
            None::<&PropertyExpression>,
            "label",
        );
        imp.category.set_expression(Some(&expression));
        imp.category.set_model(Some(&list_store));

        let selected_position = categories
            .iter()
            .enumerate()
            .find_map(|(i, category)| {
                if category.category_id == current_category_id {
                    Some(i + 1) // +1 because of 'None' at the top of the list
                } else {
                    None
                }
            })
            .unwrap_or(0);
        imp.category.set_selected(selected_position as u32);

        imp.category.connect_selected_item_notify(clone!(
            #[weak(rename_to = dialog)]
            self,
            #[upgrade_or_panic]
            move |combo| {
                if let Some(selected) = combo
                    .selected_item()
                    .and_then(|obj| obj.downcast::<CategorySelectGObject>().ok())
                {
                    let imp = dialog.imp();
                    let mut mapping = None;
                    if let Some(current_mapping) = imp.current_mapping.borrow().as_ref() {
                        let category_id = selected.id();
                        let new_mapping = FeedMapping {
                            feed_id: current_mapping.feed_id.clone(),
                            category_id: category_id.clone(),
                            sort_index: None,
                        };
                        App::default().move_feed(current_mapping.clone(), new_mapping.clone());
                        mapping = Some(new_mapping);
                    }

                    imp.current_mapping.replace(mapping);
                }
            }
        ));
    }

    fn setup_mathjax_widgets(&self, feed: &Feed) {
        let imp = self.imp();

        if let Some(feed_settings) = App::default().settings().read().get_feed_settings(&feed.feed_id) {
            imp.inline_delimiter
                .set_text(feed_settings.inline_math.as_deref().unwrap_or(""));
        }

        imp.inline_delimiter.connect_apply(clone!(
            #[strong]
            feed,
            move |entry| {
                let mut feed_settings = App::default()
                    .settings()
                    .read()
                    .get_feed_settings(&feed.feed_id)
                    .unwrap_or_default();
                if entry.text().is_empty() {
                    feed_settings.inline_math = None;
                } else {
                    feed_settings.inline_math = Some(entry.text().as_str().into());
                }

                _ = App::default()
                    .settings()
                    .write()
                    .set_feed_settings(&feed.feed_id, &feed_settings);
            }
        ));
    }

    fn setup_scrap_content_widgets(&self, feed: &Feed) {
        let imp = self.imp();

        if let Some(feed_settings) = App::default().settings().read().get_feed_settings(&feed.feed_id) {
            imp.scrap_content_switch.set_active(feed_settings.scrap_content);
        }

        imp.scrap_content_switch.connect_state_set(clone!(
            #[strong]
            feed,
            move |_switch, is_set| {
                let mut feed_settings = App::default()
                    .settings()
                    .read()
                    .get_feed_settings(&feed.feed_id)
                    .unwrap_or_default();
                feed_settings.scrap_content = is_set;
                _ = App::default()
                    .settings()
                    .write()
                    .set_feed_settings(&feed.feed_id, &feed_settings);
                Propagation::Proceed
            }
        ));
    }
}

mod obj_impl {
    use super::*;

    #[derive(Properties)]
    #[properties(wrapper_type = super::CategorySelectGObject)]
    pub struct CategorySelectGObject {
        pub id: RefCell<CategoryID>,
        #[property(name = "label", get, set)]
        pub label: RefCell<String>,
    }

    impl Default for CategorySelectGObject {
        fn default() -> Self {
            Self {
                id: RefCell::new(NEWSFLASH_TOPLEVEL.clone()),
                label: RefCell::new(String::new()),
            }
        }
    }

    impl ObjectImpl for CategorySelectGObject {
        fn properties() -> &'static [ParamSpec] {
            Self::derived_properties()
        }
        fn set_property(&self, id: usize, value: &Value, pspec: &ParamSpec) {
            self.derived_set_property(id, value, pspec)
        }
        fn property(&self, id: usize, pspec: &ParamSpec) -> Value {
            self.derived_property(id, pspec)
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for CategorySelectGObject {
        const NAME: &'static str = "CategorySelectGObject";
        type Type = super::CategorySelectGObject;
    }
}

glib::wrapper! {
    pub struct CategorySelectGObject(ObjectSubclass<obj_impl::CategorySelectGObject>);
}

impl Default for CategorySelectGObject {
    fn default() -> Self {
        Object::new()
    }
}

impl CategorySelectGObject {
    pub fn new(id: &CategoryID, label: &str) -> Self {
        let obj = Self::default();
        let imp = obj.imp();
        imp.id.replace(id.clone());
        imp.label.replace(label.into());
        obj
    }

    pub fn from_category(category: &Category) -> Self {
        let obj = Self::default();
        let imp = obj.imp();
        imp.id.replace(category.category_id.clone());
        imp.label.replace(category.label.clone());
        obj
    }

    pub fn id(&self) -> Ref<CategoryID> {
        self.imp().id.borrow()
    }
}
