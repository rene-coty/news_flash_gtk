use chrono::{DateTime, Duration, Utc};
use gdk4::Texture;
use gtk4::Image;
use moka::future::Cache;
use news_flash::{models::FeedID, NewsFlash};
use reqwest::Client;
use std::sync::Arc;
use tokio::sync::RwLock;

use crate::app::App;

#[derive(Debug, Clone)]
pub struct FaviconCacheEntry {
    pub texture: Option<Texture>,
    pub expires: DateTime<Utc>,
}

impl FaviconCacheEntry {
    pub fn is_expired(&self) -> bool {
        Utc::now() >= self.expires
    }
}

pub struct FaviconCache {
    cache: Cache<FeedID, FaviconCacheEntry>,
}

impl Default for FaviconCache {
    fn default() -> Self {
        Self {
            cache: Cache::new(Self::CACHE_SIZE),
        }
    }
}

impl FaviconCache {
    const CACHE_SIZE: u64 = 99999;
    const EXPIRES_AFTER_DAYS: i64 = 30;

    pub fn new() -> Self {
        Self::default()
    }

    pub fn get_icon(&self, feed_id: &FeedID, image: &Image) {
        let feed_id = feed_id.clone();
        let image = image.clone();
        let cache = self.cache.clone();

        App::default().execute_with_callback(
            |news_flash, client| async move {
                let init_future = Self::init_icon(&feed_id, news_flash, client);
                let entry = cache
                    .entry_by_ref(&feed_id)
                    .or_insert_with_if(init_future, |entry| entry.is_expired())
                    .await;
                entry.into_value()
            },
            move |_app, entry| {
                if entry.texture.is_some() {
                    image.set_paintable(entry.texture.as_ref());
                }
            },
        );
    }

    async fn init_icon(
        feed_id: &FeedID,
        news_flash: Arc<RwLock<Option<NewsFlash>>>,
        client: Client,
    ) -> FaviconCacheEntry {
        let favicon = if let Some(news_flash) = news_flash.read().await.as_ref() {
            news_flash.get_icon(feed_id, &client).await.ok()
        } else {
            None
        };

        let expires = favicon
            .as_ref()
            .map(|favicon| favicon.expires)
            .unwrap_or(Utc::now() + Duration::try_days(Self::EXPIRES_AFTER_DAYS).unwrap());

        let texture = favicon.and_then(|icon| icon.data).and_then(|data| {
            let bytes = glib::Bytes::from_owned(data);
            gdk4::Texture::from_bytes(&bytes).ok()
        });

        FaviconCacheEntry { texture, expires }
    }
}
