mod service_row;

use self::service_row::ServiceRow;
use crate::app::App;
use glib::{clone, subclass};
use gtk4::{prelude::*, subclass::prelude::*};
use gtk4::{CompositeTemplate, ListBox, ListBoxRow};
use news_flash::models::{LoginData, LoginGUI, PluginID, PluginInfo, ServiceType};
use news_flash::NewsFlash;
use std::cell::RefCell;
use std::collections::HashMap;
use std::rc::Rc;

mod imp {
    use super::*;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(file = "data/resources/ui_templates/login/welcome.blp")]
    pub struct WelcomePage {
        pub services: Rc<RefCell<HashMap<PluginID, PluginInfo>>>,

        #[template_child]
        pub local_list: TemplateChild<ListBox>,
        #[template_child]
        pub service_list: TemplateChild<ListBox>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for WelcomePage {
        const NAME: &'static str = "WelcomePage";
        type ParentType = gtk4::Box;
        type Type = super::WelcomePage;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for WelcomePage {
        fn constructed(&self) {
            self.obj().init();
        }
    }

    impl WidgetImpl for WelcomePage {}

    impl BoxImpl for WelcomePage {}
}

glib::wrapper! {
    pub struct WelcomePage(ObjectSubclass<imp::WelcomePage>)
        @extends gtk4::Widget, gtk4::Box;
}

impl Default for WelcomePage {
    fn default() -> Self {
        glib::Object::new::<Self>()
    }
}

impl WelcomePage {
    pub fn new() -> Self {
        Self::default()
    }

    fn init(&self) {
        self.populate();
        self.connect_signals();
    }

    fn populate(&self) {
        let services = NewsFlash::list_backends();
        let imp = self.imp();

        for (_id, plugin_info) in services.iter() {
            let row = ServiceRow::new(plugin_info);

            if plugin_info.service_type == ServiceType::Local {
                imp.local_list.append(&row);
            } else {
                imp.service_list.append(&row);
            }
        }

        imp.services.replace(services);
    }

    fn connect_signals(&self) {
        let imp = self.imp();

        imp.service_list.connect_row_activated(clone!(
            #[strong(rename_to = services)]
            imp.services,
            move |_list, row| {
                Self::row_selected(row, &services);
            }
        ));
        imp.local_list.connect_row_activated(clone!(
            #[strong(rename_to = services)]
            imp.services,
            move |_list, row| {
                Self::row_selected(row, &services);
            }
        ));
    }

    fn row_selected(row: &ListBoxRow, services: &Rc<RefCell<HashMap<PluginID, PluginInfo>>>) {
        if let Some(service_row) = row.downcast_ref::<ServiceRow>() {
            let plugin_id = service_row.plugin_id();
            if let Some(plugin_info) = services.borrow().get(&plugin_id) {
                match plugin_info.login_gui {
                    LoginGUI::OAuth(_) | LoginGUI::Direct(_) => {
                        App::default().main_window().show_login_page(&plugin_id, None)
                    }
                    LoginGUI::None => {
                        App::default().login(LoginData::None(plugin_id));
                    }
                }
            }
        }
    }
}
