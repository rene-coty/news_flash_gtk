use crate::sidebar::FeedListItemID;
use news_flash::models::TagID;
use serde::{Deserialize, Serialize};
use std::sync::Arc;

#[derive(Clone, Default, Debug, Serialize, Deserialize)]
pub enum SidebarSelection {
    #[default]
    All,
    FeedList(FeedListItemID, Arc<String>),
    Tag(TagID, Arc<String>),
}

impl SidebarSelection {
    pub fn from_feed_list_selection(selection: FeedListItemID, title: String) -> Self {
        SidebarSelection::FeedList(selection, Arc::new(title))
    }

    pub fn from_tag_list_selection(selection: TagID, title: String) -> Self {
        SidebarSelection::Tag(selection, Arc::new(title))
    }
}

impl PartialEq for SidebarSelection {
    fn eq(&self, other: &SidebarSelection) -> bool {
        match self {
            SidebarSelection::All => matches!(other, SidebarSelection::All),
            SidebarSelection::FeedList(self_id, _title) => match other {
                SidebarSelection::FeedList(other_id, _title) => self_id == other_id,
                _ => false,
            },
            SidebarSelection::Tag(self_id, _title) => match other {
                SidebarSelection::Tag(other_id, _title) => self_id == other_id,
                _ => false,
            },
        }
    }
}
