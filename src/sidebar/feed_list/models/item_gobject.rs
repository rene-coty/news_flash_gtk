use super::{FeedListCategoryModel, FeedListFeedModel, FeedListItem, FeedListItemID};
use gio::{prelude::*, ListModel, ListStore};
use glib::{Boxed, Object, ParamSpec, ParamSpecString, ParamSpecUInt, Value};
use gtk4::subclass::prelude::*;
use news_flash::models::CategoryID;
use once_cell::sync::Lazy;
use std::cell::{Cell, RefCell};
use std::collections::HashMap;
use std::rc::Rc;

mod imp {
    use super::*;

    pub struct FeedListItemGObject {
        pub id: RefCell<FeedListItemID>,
        pub parent_id: RefCell<CategoryID>,
        pub item_count: Cell<u32>,
        pub error_message: RefCell<Rc<String>>,
        pub label: RefCell<Rc<String>>,
        pub sort_index: Cell<i32>,
        pub list_store: RefCell<Option<ListStore>>,
    }

    impl Default for FeedListItemGObject {
        fn default() -> Self {
            Self {
                id: RefCell::new(FeedListItemID::Category(CategoryID::new(""))),
                parent_id: RefCell::new(CategoryID::new("")),
                item_count: Cell::new(0),
                error_message: RefCell::new(Rc::new(String::default())),
                label: RefCell::new(Rc::new(String::default())),
                sort_index: Cell::new(0),
                list_store: RefCell::new(None),
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for FeedListItemGObject {
        const NAME: &'static str = "FeedListItemGObject";
        type Type = super::FeedListItemGObject;
    }

    impl ObjectImpl for FeedListItemGObject {
        fn properties() -> &'static [ParamSpec] {
            static PROPERTIES: Lazy<Vec<ParamSpec>> = Lazy::new(|| {
                vec![
                    ParamSpecUInt::builder("item-count").build(),
                    ParamSpecString::builder("label").build(),
                    ParamSpecString::builder("error-message").build(),
                ]
            });
            PROPERTIES.as_ref()
        }

        fn set_property(&self, _id: usize, value: &Value, pspec: &ParamSpec) {
            match pspec.name() {
                "item-count" => {
                    let input = value.get().expect("The value needs to be of type `u32`.");
                    self.item_count.set(input);
                }
                "label" => {
                    let input = value.get().expect("The value needs to be of type `string`.");
                    self.label.replace(Rc::new(input));
                }
                "error-message" => {
                    let input = value.get().expect("The value needs to be of type `string`.");
                    self.error_message.replace(Rc::new(input));
                }
                _ => unreachable!(),
            }
        }

        fn property(&self, _id: usize, pspec: &ParamSpec) -> Value {
            match pspec.name() {
                "item-count" => self.item_count.get().to_value(),
                "label" => (*self.label.borrow()).to_value(),
                "error-message" => (*self.error_message.borrow()).to_value(),
                _ => unreachable!(),
            }
        }
    }
}

glib::wrapper! {
    pub struct FeedListItemGObject(ObjectSubclass<imp::FeedListItemGObject>);
}

impl Default for FeedListItemGObject {
    fn default() -> Self {
        Object::new()
    }
}

impl FeedListItemGObject {
    pub fn new() -> Self {
        Self::default()
    }

    pub fn from_category(
        model: &FeedListCategoryModel,
        model_index: &mut HashMap<FeedListItemID, FeedListItemGObject>,
    ) -> Self {
        let gobject = Self::new();
        let imp = gobject.imp();

        imp.id.replace(model.id.clone());
        imp.label.replace(Rc::new(model.label.clone()));
        imp.parent_id.replace(model.parent_id.clone());
        imp.item_count.set(model.item_count as u32);
        imp.sort_index.set(model.sort_index);

        let list_store = ListStore::new::<FeedListItemGObject>();
        for child in &model.children {
            match child {
                FeedListItem::Feed(feed) => {
                    let item = FeedListItemGObject::from_feed(feed);
                    list_store.append(&item);
                    model_index.insert(item.id(), item);
                }
                FeedListItem::Category(category) => {
                    let item = FeedListItemGObject::from_category(category, model_index);
                    list_store.append(&item);
                    model_index.insert(item.id(), item);
                }
            }
        }
        imp.list_store.replace(Some(list_store));

        gobject
    }

    pub fn from_feed(model: &FeedListFeedModel) -> Self {
        let gobject = Self::new();
        let imp = gobject.imp();

        imp.id.replace(model.id.clone());
        imp.label.replace(Rc::new(model.label.clone()));
        imp.parent_id.replace(model.parent_id.clone());
        imp.item_count.set(model.item_count as u32);
        imp.sort_index.set(model.sort_index);
        imp.error_message.replace(Rc::new(model.error_message.clone()));

        gobject
    }

    pub fn id(&self) -> FeedListItemID {
        self.imp().id.borrow().clone()
    }

    pub fn parent_id(&self) -> CategoryID {
        self.imp().parent_id.borrow().clone()
    }

    pub fn item_count(&self) -> u32 {
        self.imp().item_count.get()
    }

    pub fn set_item_count(&self, item_count: u32) {
        self.set_property("item-count", item_count.to_value());
    }

    pub fn error_message(&self) -> Rc<String> {
        self.imp().error_message.borrow().clone()
    }

    pub fn set_error_message(&self, error_message: &str) {
        self.set_property("error-message", error_message.to_value());
    }

    pub fn sort_index(&self) -> i32 {
        self.imp().sort_index.get()
    }

    pub fn label(&self) -> Rc<String> {
        self.imp().label.borrow().clone()
    }

    pub fn set_label(&self, label: &str) {
        self.set_property("label", label.to_value());
    }

    pub fn is_feed(&self) -> bool {
        self.imp().id.borrow().is_feed()
    }

    pub fn is_category(&self) -> bool {
        self.imp().id.borrow().is_category()
    }

    pub fn children_model(&self) -> Option<ListModel> {
        self.imp()
            .list_store
            .borrow()
            .clone()
            .map(|model| model.upcast::<ListModel>())
    }

    pub fn children_list_store(&self) -> Option<ListStore> {
        self.imp().list_store.borrow().clone()
    }

    pub fn remove_from_index(&self, model_index: &mut HashMap<FeedListItemID, FeedListItemGObject>) {
        if let Some(list_store) = self.imp().list_store.borrow().as_ref() {
            let n_items = list_store.n_items();
            for i in 0..n_items {
                if let Some(item) = list_store.item(i) {
                    if let Ok(item_gobject) = item.downcast::<FeedListItemGObject>() {
                        item_gobject.remove_from_index(model_index);
                        model_index.remove(&item_gobject.id());
                    }
                }
            }
        }
    }
}

#[derive(Clone, Debug, PartialEq, Eq, Boxed)]
#[boxed_type(name = "GFeedListItemID")]
pub struct GFeedListItemID(FeedListItemID);

impl From<FeedListItemID> for GFeedListItemID {
    fn from(id: FeedListItemID) -> Self {
        Self(id)
    }
}

impl From<GFeedListItemID> for FeedListItemID {
    fn from(id: GFeedListItemID) -> Self {
        id.0
    }
}
