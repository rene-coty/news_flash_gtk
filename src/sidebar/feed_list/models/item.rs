use crate::{app::App, settings::GFeedOrder};

use super::category::FeedListCategoryModel;
use super::feed::FeedListFeedModel;
use diffus::{Diffus, Same};
use news_flash::models::{CategoryID, FeedMapping};
use serde::{Deserialize, Serialize};
use std::{cmp::Ordering, fmt, sync::Arc};

#[derive(Eq, Clone, Debug, Hash, Serialize, Deserialize)]
pub enum FeedListItemID {
    Category(CategoryID),
    Feed(Arc<FeedMapping>),
}

impl FeedListItemID {
    pub fn is_category(&self) -> bool {
        matches!(self, FeedListItemID::Category(..))
    }

    pub fn is_feed(&self) -> bool {
        matches!(self, FeedListItemID::Feed(..))
    }
}

impl fmt::Display for FeedListItemID {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::Category(id) => write!(f, "{id}"),
            Self::Feed(mapping) => write!(f, "{} - parent {}", mapping.feed_id, mapping.category_id),
        }
    }
}

impl PartialEq for FeedListItemID {
    fn eq(&self, other: &FeedListItemID) -> bool {
        match other {
            FeedListItemID::Category(other_category) => match self {
                FeedListItemID::Category(self_category) => other_category == self_category,
                FeedListItemID::Feed(_) => false,
            },
            FeedListItemID::Feed(other_mapping) => match self {
                FeedListItemID::Feed(self_mapping) => other_mapping == self_mapping,
                FeedListItemID::Category(_) => false,
            },
        }
    }
}

#[derive(Diffus, Eq, Clone, Debug)]
pub enum FeedListItem {
    Feed(FeedListFeedModel),
    Category(FeedListCategoryModel),
}

impl FeedListItem {
    pub fn id(&self) -> FeedListItemID {
        match self {
            Self::Feed(feed) => feed.id.clone(),
            Self::Category(category) => category.id.clone(),
        }
    }

    pub fn label(&self) -> &str {
        match self {
            Self::Feed(feed) => &feed.label,
            Self::Category(category) => &category.label,
        }
    }

    pub fn item_count(&self) -> i64 {
        match self {
            Self::Feed(feed) => feed.item_count,
            Self::Category(category) => category.item_count,
        }
    }
}

impl Same for FeedListItem {
    fn same(&self, other: &Self) -> bool {
        match self {
            FeedListItem::Feed(self_feed) => match other {
                FeedListItem::Feed(other_feed) => self_feed.same(other_feed),
                FeedListItem::Category(_other_category) => false,
            },
            FeedListItem::Category(self_category) => match other {
                FeedListItem::Feed(_other_feed) => false,
                FeedListItem::Category(other_category) => self_category.same(other_category),
            },
        }
    }
}

impl PartialEq for FeedListItem {
    fn eq(&self, other: &FeedListItem) -> bool {
        match other {
            FeedListItem::Category(other_category) => match self {
                FeedListItem::Category(self_category) => other_category.id == self_category.id,
                FeedListItem::Feed(_) => false,
            },
            FeedListItem::Feed(other_feed) => match self {
                FeedListItem::Feed(self_feed) => other_feed.id == self_feed.id,
                FeedListItem::Category(_) => false,
            },
        }
    }
}

impl Ord for FeedListItem {
    fn cmp(&self, other: &FeedListItem) -> Ordering {
        let order = App::default().settings().read().get_feed_list_order();
        match self {
            FeedListItem::Feed(self_feed) => match other {
                FeedListItem::Feed(other_feed) => match order {
                    GFeedOrder::Manual => self_feed.sort_index.cmp(&other_feed.sort_index),
                    GFeedOrder::Alphabetical => self_feed.label.to_lowercase().cmp(&other_feed.label.to_lowercase()),
                },
                FeedListItem::Category(other_category) => match order {
                    GFeedOrder::Manual => self_feed.sort_index.cmp(&other_category.sort_index),
                    GFeedOrder::Alphabetical => Ordering::Greater, // categories before feeds in  case of alphabetical
                },
            },
            FeedListItem::Category(self_category) => match other {
                FeedListItem::Feed(other_feed) => match order {
                    GFeedOrder::Manual => self_category.sort_index.cmp(&other_feed.sort_index),
                    GFeedOrder::Alphabetical => Ordering::Less, // categories before feeds in  case of alphabetical
                },
                FeedListItem::Category(other_category) => match order {
                    GFeedOrder::Manual => self_category.sort_index.cmp(&other_category.sort_index),
                    GFeedOrder::Alphabetical => self_category
                        .label
                        .to_lowercase()
                        .cmp(&other_category.label.to_lowercase()),
                },
            },
        }
    }
}

impl PartialOrd for FeedListItem {
    fn partial_cmp(&self, other: &FeedListItem) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}
