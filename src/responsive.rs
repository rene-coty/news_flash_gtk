use crate::content_page::ContentPage;
use glib::clone;
use gtk4::prelude::*;

#[derive(Clone, Debug)]
pub struct ResponsiveLayout {
    pub content_page: ContentPage,
}

impl ResponsiveLayout {
    pub fn new(content_page: &ContentPage) -> Self {
        let layout = ResponsiveLayout {
            content_page: content_page.clone(),
        };
        layout.setup_sidebar_reveal_button();
        layout
    }

    fn setup_sidebar_reveal_button(&self) {
        self.content_page.outer().connect_collapsed_notify(clone!(
            #[strong(rename_to = this)]
            self,
            move |outer| {
                this.content_page
                    .article_list_column()
                    .show_sidebar_button()
                    .set_visible(outer.is_collapsed());
            }
        ));

        self.content_page
            .article_list_column()
            .show_sidebar_button()
            .connect_toggled(clone!(
                #[strong(rename_to = this)]
                self,
                move |button| {
                    this.content_page.outer().set_show_sidebar(button.is_active());
                }
            ));

        self.content_page.outer().connect_show_sidebar_notify(clone!(
            #[strong(rename_to = this)]
            self,
            move |outer| {
                this.content_page
                    .article_list_column()
                    .show_sidebar_button()
                    .set_active(outer.shows_sidebar());
            }
        ));
    }

    pub fn show_sidebar(&self) {
        self.content_page
            .article_list_column()
            .show_sidebar_button()
            .set_active(true);
    }

    pub fn show_article_view(&self, show: bool) {
        self.content_page.inner().set_show_content(show);
    }
}
