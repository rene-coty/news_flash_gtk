use crate::add_dialog::g_feed::GFeed;
use crate::app::App;
use crate::i18n::i18n;
use glib::{clone, subclass, subclass::*};
use gtk4::{prelude::*, subclass::prelude::*, Box, Button, CheckButton, CompositeTemplate, Stack, Widget};
use libadwaita::{prelude::*, ActionRow, PreferencesGroup};
use once_cell::sync::Lazy;

use news_flash::{
    models::{Feed, FeedID, Url},
    ParsedUrl,
};
use std::{cell::RefCell, rc::Rc, sync::Arc};
use tokio::sync::Semaphore;

mod imp {
    use super::*;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(file = "data/resources/ui_templates/add_dialog/select_feed.blp")]
    pub struct SelectFeedWidget {
        #[template_child]
        pub feed_list: TemplateChild<PreferencesGroup>,
        #[template_child]
        pub select_button: TemplateChild<Button>,
        #[template_child]
        pub select_button_stack: TemplateChild<Stack>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for SelectFeedWidget {
        const NAME: &'static str = "SelectFeedWidget";
        type ParentType = Box;
        type Type = super::SelectFeedWidget;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for SelectFeedWidget {
        fn signals() -> &'static [Signal] {
            static SIGNALS: Lazy<Vec<Signal>> = Lazy::new(|| {
                vec![
                    Signal::builder("selected").param_types([GFeed::static_type()]).build(),
                    Signal::builder("error").param_types([String::static_type()]).build(),
                ]
            });
            SIGNALS.as_ref()
        }
    }

    impl WidgetImpl for SelectFeedWidget {}

    impl BoxImpl for SelectFeedWidget {}
}

glib::wrapper! {
    pub struct SelectFeedWidget(ObjectSubclass<imp::SelectFeedWidget>)
        @extends Widget, Box;
}

impl Default for SelectFeedWidget {
    fn default() -> Self {
        glib::Object::new::<Self>()
    }
}

impl SelectFeedWidget {
    pub fn new() -> Self {
        Self::default()
    }

    pub fn fill(&self, feed_vec: Vec<Feed>) {
        let imp = self.imp();
        let selected_feed_url: Rc<RefCell<Option<Url>>> = Rc::new(RefCell::new(None));

        imp.select_button.connect_clicked(clone!(
            #[weak(rename_to = widget)]
            self,
            #[strong]
            selected_feed_url,
            #[upgrade_or_panic]
            move |button| {
                let imp = widget.imp();

                if let Some(feed_url) = selected_feed_url.take() {
                    imp.select_button_stack.set_visible_child_name("spinner");
                    button.set_sensitive(false);

                    let feed_id = FeedID::new(feed_url.as_str());

                    App::default().execute_with_callback(
                        |news_flash, client| async move {
                            let semaphore = news_flash
                                .read()
                                .await
                                .as_ref()
                                .map(|x| x.get_semaphore())
                                .unwrap_or(Arc::new(Semaphore::new(1)));
                            news_flash::feed_parser::download_and_parse_feed(
                                &feed_url, &feed_id, None, semaphore, &client,
                            )
                            .await
                        },
                        clone!(
                            #[weak]
                            widget,
                            #[upgrade_or_panic]
                            move |_app, res| {
                                let imp = widget.imp();

                                if let Ok(ParsedUrl::SingleFeed(feed)) = res {
                                    widget.emit_by_name::<()>("selected", &[&GFeed::from(*feed)]);
                                } else {
                                    widget.emit_by_name::<()>("error", &[&i18n("Can't parse Feed")]);
                                }

                                imp.select_button_stack.set_visible_child_name("text");
                                imp.select_button.set_sensitive(true);
                            }
                        ),
                    );
                }
            }
        ));
        let mut group: Option<CheckButton> = None;
        for feed in feed_vec {
            let radio_button = CheckButton::new();
            radio_button.set_group(group.as_ref());

            let select_button = imp.select_button.get();
            radio_button.connect_toggled(clone!(
                #[strong(rename_to = feed_url)]
                feed.feed_url,
                #[strong]
                selected_feed_url,
                move |radio_button| {
                    if radio_button.is_active() {
                        selected_feed_url.replace(feed_url.clone());
                    }
                    select_button.set_sensitive(true);
                }
            ));

            let row = ActionRow::new();
            row.set_title(&feed.label);
            row.set_activatable(true);
            row.set_activatable_widget(Some(&radio_button));
            row.add_prefix(&radio_button);
            if let Some(url) = feed.feed_url {
                row.set_widget_name(url.as_str());
            }

            imp.feed_list.add(&row);

            group = Some(radio_button);
        }
    }
}
