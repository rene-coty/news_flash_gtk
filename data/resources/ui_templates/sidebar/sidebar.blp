using Gtk 4.0;

template $SideBar : Box {
  width-request: 200;
  vexpand: true;
  orientation: vertical;

  DropControllerMotion drop_motion {}
  GestureDrag drag_gesture {}

  ScrolledWindow sidebar_scroll {
    hexpand: true;
    vexpand: true;
    hscrollbar-policy: never;

    Viewport {
      Box {
        orientation: vertical;

        ListBox all_articles_fake_list {
          margin-top: 10;
          selection-mode: single;

          ListBoxRow all_articles_row {
            Box {
              spacing: 2;

              Label {
                hexpand: true;
                halign: start;
                label: _("All Articles");

                styles [
                  "heading",
                ]
              }

              Label item_count_all_label {
                width-request: 24;
                valign: center;
                label: "0";

                styles [
                  "item-count",
                ]
              }

              GestureClick all_articles_click {
                button: 3;
              }

              GestureLongPress all_articles_press {}

              PopoverMenu all_articles_menu {
                menu-model: all_articles_popover;
                has-arrow: false;
                halign: start;
              }
            }
          }

          styles [
            "navigation-sidebar",
          ]
        }

        Box {
          orientation: vertical;

          Box subscriptions_event_box {
            height-request: 25;
            margin-top: 15;
            spacing: 5;

            EventControllerMotion subscriptions_motion {
            }

            GestureClick subscriptions_click {
              button: 1;
            }

            Label {
              margin-start: 16;
              hexpand: true;
              halign: start;
              label: _("Subscriptions");

              styles [
                "dim-label",
                "caption-heading",
              ]
            }

            [end]
            Image subscriptions_expander {
              width-request: 24;
              height-request: 24;
              margin-end: 13;
              icon-name: "go-previous-symbolic";
              opacity: 0.5;

              styles [
                "symbolic",
                "backward-arrow-expanded",
              ]
            }
          }

          Revealer subscriptions_revealer {
            reveal-child: true;
            child: $FeedList feed_list {};
          }
        }

        Box tags_box {
          orientation: vertical;

          Box tags_event_box {
            height-request: 25;
            margin-top: 10;
            spacing: 5;

            Label {
              margin-start: 16;
              hexpand: true;
              halign: start;
              label: _("Tags");

              styles [
                "dim-label",
                "caption-heading",
              ]
            }

            [end]
            Image tags_expander {
              margin-end: 13;
              width-request: 24;
              height-request: 24;
              icon-name: "go-previous-symbolic";
              opacity: 0.5;

              styles [
                "symbolic",
                "backward-arrow-expanded",
              ]
            }

            EventControllerMotion tags_motion {
            }

            GestureClick tags_click {
              button: 1;
            }
          }

          Revealer tags_revealer {
            reveal-child: true;
            child: $TagList tag_list {};
          }
        }
      }
    }
  }
}

menu all_articles_popover {
  item {
    label: _("Set Read");
    action: "win.mark-all-read";
  }
}