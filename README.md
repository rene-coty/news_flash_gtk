[![Translation status](https://hosted.weblate.org/widgets/newsflash/-/svg-badge.svg)](https://hosted.weblate.org/engage/newsflash/?utm_source=widget)

<a href='https://circle.gnome.org/'><img width='100' alt='Gnome Circle' src='https://gitlab.gnome.org/Teams/Circle/-/raw/master/assets/button/circle-button-i.svg'/></a>

# Newsflash

(The spiritual successor to [FeedReader](https://github.com/jangernert/FeedReader))

Follow your favorite blogs & news sites.

Newsflash is a program designed to complement an already existing web-based RSS reader account.
It combines all the advantages of web based services like syncing across all your devices with everything you expect
from a modern desktop program: Desktop notifications, fast search and filtering, tagging, handy keyboard shortcuts
and having access to all your articles as long as you like.

![Screenshot](./data/screenshots/Main.png "WIP 2020-04-20")

## Flathub

Official package available on Flathub:

<a href='https://flathub.org/apps/details/io.gitlab.news_flash.NewsFlash'><img width='240' alt='Download on Flathub' src='https://flathub.org/api/badge?svg&locale=en'/></a>

## Unsupported Packages

These packages might be outdated and have issues not present in the official flatpak. Use at your own risk and verify an issue is reproducible with the flatpak before filing a report.

### Alpine

```bash
doas apk add newsflash
```

### Nix

```bash
nix-shell -p newsflash
```

### Snap

[![Snap](https://snapcraft.io/static/images/badges/en/snap-store-black.svg)](https://snapcraft.io/newsflash)

## Looking for service maintainers

I'm looking for people that are actively using a specific service backend of Newsflash and are willing to maintain it.
The size of the code for each service is quite managable. But keeping an eye on and testing every service can be quite challenging.
So this time around I'm hoping to find at least one person per service that knows the basics of rust and uses the service on a (almost) daily basis.

Services & Maintainers:

- Miniflux: JanGernert (me)
- ~~feedly: Alessio Biancalana ([@dottorblaster](https://gitlab.com/dottorblaster))~~ (API secret expired, see [news\_flash#66](https://gitlab.com/news_flash/news_flash/-/issues/66))
- local RSS: Kira Bruneau ([@MetaDark](https://gitlab.com/MetaDark))
- fever: Felix Bühler ([@Stunkymonkey](https://gitlab.com/Stunkymonkey))
- FreshRSS: Felix Bühler ([@Stunkymonkey](https://gitlab.com/Stunkymonkey))
- NewsBlur: Alistair Francis ([@alistair23](https://gitlab.com/alistair23))
- Inoreader: still looking
- feedbin: still looking
- Nextcloud News: still looking

We now have a [guide](https://gitlab.com/news_flash/news_flash/-/blob/master/docs/integrate_new_service.md) and [code template](https://gitlab.com/news_flash/news_flash/-/tree/master/src/feed_api_implementations/template) to get you started implementing new services.

# Grab full articles

Newsflash has a built-in content grabber that can in a lot of cases download & extract the full article from the web. This feature does not get around paywalls. It does however lessen the ad revenue of the website. So make sure to support your favorite small sites in other ways.

The grabber first checks for custom rules for the specific website and falls back to the mozilla readability algorithm if no custom rules can be found. A lot of these rules are shiped by default. But you can provide your own by dropping a text file with the domain name and `.txt` ending into `~/.var/app/io.gitlab.news_flash.NewsFlash/config/news-flash/ftr-site-config/`. For documentation on how to write custom rules and examples go to [ftr-site-config](https://github.com/fivefilters/ftr-site-config). Don't forget to upstream your new files and improvements when you're done :)

## Compile
**!!! This is not a supported way of installing the application for normal use. Please use flatpak for that purpose !!!**

Make sure you have the development, *-dev, or *-devel, libraries of:

- [gtk4](https://gitlab.gnome.org/GNOME/gtk/)
- [webkit2gtk](https://webkitgtk.org/reference/webkit2gtk/stable/)
- [libadwaita](https://gitlab.gnome.org/GNOME/libadwaita)
- [sqlite3](https://www.sqlite.org/index.html)
- [gettext](https://www.gnu.org/software/gettext/)
- [openssl](https://www.openssl.org/)
- [clapper](https://github.com/Rafostar/clapper)

Additionally the following non-devel packages are required:

- [rust](https://rustup.rs/)
- [meson](https://mesonbuild.com/)
- [xdg-utils](https://gitlab.freedesktop.org/xdg/xdg-utils)
- [blueprint-compiler](https://gitlab.gnome.org/jwestman/blueprint-compiler/-/tree/main)


```
git clone https://gitlab.com/news-flash/news_flash_gtk.git
cd news_flash_gtk
meson --prefix=/usr build
ninja -C build
sudo ninja -C build install
```



## Migrate from FeedReader

Although Newsflash is the **spiritual** successor to FeedReader, it is a different application. There are differences and most of them are on purpose.

For services that are supported by Newsflash as well, migration is as easy as can be: just log into your account and sync away.

Some services supported by FeedReader have not yet found their way into Newsflash. Work for Google Reader style APIs is already on the way (The Old Reader).

For local RSS the best way is to export an OPML file from FeedReader and import it into Newsflash. Sadly FeedReader never gained the capability to export OPML.
@hfiguiere came to save the day and wrote an [external tool](https://gitlab.gnome.org/hub/feedreader-export) to extract an OPML file from the FeedReader database.

